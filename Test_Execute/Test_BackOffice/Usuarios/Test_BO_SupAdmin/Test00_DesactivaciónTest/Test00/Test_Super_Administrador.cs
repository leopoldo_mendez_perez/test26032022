﻿using System;
using System.IO;
using Test_Execute.Selenium;

namespace TestSuperAdministradors
{
    class SuperAdministrador
    {
        string ruta = @"C:\Logs_TE_TER\";
        string Orden;
        static String[] part; // variable para almacenar las cadenas desintegradas
        static Selenium Functions = new Selenium();
        /*
        * Fecha: 21.02.22
        * Creador: Leopoldo Mendez Perez (QA)
        * Objetivo: Desactivación de test Dependiendo el estado, si lo que se recive es 0 desactivacion/1 Acivacipón
        * Ultima actualización:
        * Responsable Ultima actualización:
        * Objetivo ultima actualización:
        */
        public void testSuperAdministrador(String textofin, String numerofin)
        {
            String DirectorioPadre = "Test_BO_SupAdmin";
            switch (textofin)
            {
                case "Login_Correcto_SupAdmin":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(DirectorioPadre, "Test01_Login_Correcto_SupAdmin", ".txt") ;
                        Console.WriteLine("Se Desactivo Test"+ textofin+"\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Acceso_Usuario_Contraseña_Incorrecta_SupAdmin":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(DirectorioPadre, "Test02_Acceso_Usuario_Contraseña_Incorrecta_SupAdmin", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Acceso_Usuario_Incorrecto_SupAdmin":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(DirectorioPadre, "Test03_Acceso_Usuario_Incorrecto_SupAdmin", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Crear_Usuario_SupAdmin":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(DirectorioPadre, "Test04_Crear_Usuario_SupAdmin", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;

                    }
                    break;
                case "Validación_campos_vácios_SupAdmin":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(DirectorioPadre, "Test05_Validación_campos_vácios_SupAdmin", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Validación_Formato_CorreoSupAdmin":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(DirectorioPadre, "Test06_Validación_Formato_CorreoSupAdmin", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Crear_Usuario_Cancelar_SupAdmin":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(DirectorioPadre, "Test07_Crear_Usuario_Cancelar_SupAdmin", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Busqueda_Usuario_Creado_SupAdmin":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(DirectorioPadre, "Test08_Busqueda_Usuario_Creado_SupAdmin", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Edición_Rol_Usuario_SupAdmin":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(DirectorioPadre, "Test09_Edición_Rol_Usuario_SupAdmin", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Validación_Rol_Editado_SupAdmin":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(DirectorioPadre, "Test10_Validación_Rol_Editado_SupAdmin", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
            }
        }
    }
}
