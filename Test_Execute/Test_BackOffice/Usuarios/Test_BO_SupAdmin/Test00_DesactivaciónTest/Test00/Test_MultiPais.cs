﻿using System;
using System.IO;
using Test_Execute.Selenium;

namespace TestEmpresa
{
    class MultiPais
    {
        string ruta = @"C:\Logs_TE_TER\";
        string Orden;
        static String[] part; // variable para almacenar las cadenas desintegradas
        static Selenium Functions = new Selenium();
        /*
        * Fecha: 22.02.22
        * Creador: Leopoldo Mendez Perez (QA)
        * Objetivo: Desactivación de test Dependiendo el estado, si lo que se recive es 0 desactivacion/1 Acivacipón
        * Ultima actualización:
        * Responsable Ultima actualización:
        * Objetivo ultima actualización:
        */
        public void testMultiPais(String textofin, String numerofin)
        {
            String directorioPadre = "Test_BO_MultiPais";
            switch (textofin)
            {
                case "Crear_Usuario_MultiPais":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test31_Crear_Usuario_MultiPais", ".txt") ;
                        Console.WriteLine("Se Desactivo Test"+ textofin+"\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Buscar_Usuario_MultiPais_Creado":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test32_Buscar_Usuario_MultiPais_Creado", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Visualizar_datos_MultiPais":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test33_Visualizar_datos_MultiPais", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Cambio_Contraseña_Usuario_MultiPais":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test34_Cambio_Contraseña_Usuario_MultiPais", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;

                    }
                    break;
                case "Asociación_Lineas_MultiPais":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test35_Asociación_Lineas_MultiPais", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Validación_Mensaje_Linea_Asociada_MultiPais":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test36_Validación_Mensaje_Linea_Asociada_MultiPais", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Desasociación_Linea_MultiPais":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test37_Desasociación_Linea_MultiPais", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Edición_Usuario_MultiPais":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test38_Edición_Usuario_MultiPais", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Busqueda_Usuario_MultiPais_Editado":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test39_Busqueda_Usuario_MultiPais_Editado", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Edición_Rol_Usuario_MultiPais":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test40_Edición_Rol_Usuario_MultiPais", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Validación_Rol_Editado_MultiPais":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test41_Validación_Rol_Editado_MultiPais", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Eliminación_Usuario_MultiPais":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test42_Eliminación_Usuario_MultiPais", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
            }
        }
    }
}
