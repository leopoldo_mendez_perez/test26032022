﻿using System;
using System.IO;
using Test_Execute.Selenium;

namespace TestAdministradors
{
    class Administrador
    {
        string ruta = @"C:\Logs_TE_TER\";
        string Orden;
        static String[] part; // variable para almacenar las cadenas desintegradas
        static Selenium Functions = new Selenium();
        /*
        * Fecha: 22.02.22
        * Creador: Leopoldo Mendez Perez (QA)
        * Objetivo: Desactivación de test Dependiendo el estado, si lo que se recive es 0 desactivacion/1 Acivacipón
        * Ultima actualización:
        * Responsable Ultima actualización:
        * Objetivo ultima actualización:
        */
        public void testAdministrador(String textofin, String numerofin)
        {
            String directorioPadre = "Test_BO_Admin";
            switch (textofin)
            {
                case "Crear_Usuario_Admin":                   
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test11_Crear_Usuario_Admin", ".txt") ;
                        Console.WriteLine("Se Desactivo Test"+ textofin+"\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Busqueda_Usuario_Admin":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test12_Busqueda_Usuario_Admin", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Validación_Campos_Vácios_Admin":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test13_Validación_Campos_Vácios_Admin", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Validación_Formato_Correo_Admin":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test14_Validación_Formato_Correo_Admin", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;

                    }
                    break;
                case "Crear_Usuario_Cancelar_Admin":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test15_Crear_Usuario_Cancelar_Admin", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Editar_Rol_Useruario_Admin":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test16_Editar_Rol_Useruario_Admin", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Vadidation_Rol_Editado_Admin":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test17_Vadidation_Rol_Editado_Admin", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Eliminar_Usuario_Admin":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test18_Eliminar_Usuario_Admin", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                
            }
        }
    }
}
