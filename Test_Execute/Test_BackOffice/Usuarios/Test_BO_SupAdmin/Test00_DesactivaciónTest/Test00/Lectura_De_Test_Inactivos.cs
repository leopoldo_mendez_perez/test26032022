﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Test_Execute.Selenium.ResxBO;
using Test_Execute.Selenium.ResxConfGlob;
using OpenQA.Selenium.Support.UI;
using System.Reflection;
using System.Configuration;
using AventStack.ExtentReports;
using TestSuperAdministradors;
using Test_Execute.Selenium;

namespace Test_Inactivos
{
    class Lectura_Test_Inactivos
    {

        string ruta = @"C:\Logs_TE_TER\";
        string Orden;
        Selenium Functions = new Selenium();
        /*
        * Fecha: 21.02.22
        * Creador: Leopoldo Mendez Perez (QA)
        * Objetivo: Leer los test Inactivos para lanzar un estado a los Test Padre para no ejecutarse
        * Ultima actualización:14-03-2022
        * Responsable Ultima actualización:Leopoldo Méndez Pérez (QA Sigel)
        * Objetivo ultima actualización:Incorporación nueva Mejora para la ejecución de test
        */
        public Boolean LecturaTestInactivos (String nombreTest)
        {

            int i = 0;
            while (Functions.TestInactivos[i, 0] != null)
            {
                Console.WriteLine("TestDesactivados=" + Functions.TestInactivos[i, 0]);
                
                if (nombreTest.Equals(Functions.TestInactivos[i, 0]))
                {
                    return true;
                    break;
                }
                i++;
            }
            return false;
        }

       
    }
}
