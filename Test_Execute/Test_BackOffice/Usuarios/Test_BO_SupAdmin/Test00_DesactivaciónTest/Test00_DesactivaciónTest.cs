﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;
using DesactivaciónTestBO;
namespace ValidacionInterfacesAuto
{
 
    public class Test00_Desactivación_BO
    {
        DesactivacónTestB0 bo = new DesactivacónTestB0();
        Selenium Functions = new Selenium();
        static ChromeDriver driver;
        /*
              * CP01-00	Desactivación de tests
              */
        [Test]
        public void DesactivaciónTestBO()
        {
            bo.DesactivaciónTestB0();
            Assert.Ignore("Test ignored during Prod runs");
        }

    }
}