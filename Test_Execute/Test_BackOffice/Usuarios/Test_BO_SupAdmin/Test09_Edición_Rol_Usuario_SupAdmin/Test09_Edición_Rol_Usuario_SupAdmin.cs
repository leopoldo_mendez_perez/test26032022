﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;
using Test_Inactivos;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test09_Edición_Rol_Usuario_SupAdmin
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        Edicion_Rol_Usuario_SuperAdministrador ed = new Edicion_Rol_Usuario_SuperAdministrador();
        static login login = new login();
        private static List<String> lista = new List<String>();
      
        Lectura_Test_Inactivos lecEstado = new Lectura_Test_Inactivos();
        /*
         * CP01-09	Editar Usuario (SuperAdministrador) Edicion de Rol
         */
        [Test]
        public void Test09_Edición_Rol_Usuario_SupAdmin_()
        {
            if (!lecEstado.LecturaTestInactivos("Edición_Rol_Usuario_SupAdmin"))
            {
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                ed.EdicionUsuarioBOClaroreado(driver, lista, LocalizadoresBO._010908_opcionSuperAdministrador, LocalizadoresBO._010909_ValidarTexto);
            }
            else
            {
                Assert.Ignore("Test ignored during Prod runs");
            }
        }

    }
}