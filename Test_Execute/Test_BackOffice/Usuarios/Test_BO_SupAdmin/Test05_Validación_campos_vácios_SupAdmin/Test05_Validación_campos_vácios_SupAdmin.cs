﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;
using Test_Inactivos;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test05_Validación_campos_vácios_SupAdmin
    {
     
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        static ValidarCamposVacios val = new ValidarCamposVacios();
        private static List<String> lista = new List<String>();
      
        string Orden; static login login = new login();
        Lectura_Test_Inactivos lecEstado = new Lectura_Test_Inactivos();
        /*
        CP01-05	Crear usuario(SuperAdministrador) Validacion de campos vacios (user)
        */
        [Test]
        public void Test05_Validación_campos_vácios_SupAdmin_()
        {
            if (!lecEstado.LecturaTestInactivos("Validación_campos_vácios_SupAdmin"))
            {
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                val.altaUsuarioBOClaroValidacionCamposVacios(driver, lista, LocalizadoresBO._010403_linkSuperAdministradores);
            }
            else
            {
                Assert.Ignore("Test ignored during Prod runs");
            }
        }

    }
}