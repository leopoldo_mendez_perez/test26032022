﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Test_Execute.Selenium.ResxBO;
using Test_Execute.Selenium.ResxConfGlob;
using OpenQA.Selenium.Support.UI;
using System.Reflection;
using System.Configuration;
using AventStack.ExtentReports;
using NUnit.Framework;

namespace Test_Execute.Selenium.suit
{
    class ValidarCamposVacios
    {

        Selenium Functions = new Selenium();
        string ruta = @"C:\Logs_TE_TER\";
       
        string Orden;



        /*
         * Fecha:21.02.22
         * Creador: Leopoldo Méndez Pérez (QA)
         * Objetivo: Método para validación de campos vacios en el formulario de alta de usuario para BO para clientes
         * Ultima actualización:
         * Responsable Última actualización:
         * Objetivo última actualización:
         */
        public void altaUsuarioBOClaroValidacionCamposVacios(ChromeDriver driver, List<String> lista, String xpath)
        {
            try { 
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010401_linkUsuarioHome, lista, "0", 10);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.scrooll(driver, LocalizadoresBO._010404_buttonCrearUsuario);
            Functions.ClickButton(driver, "Id", LocalizadoresBO._010402_buttonList, lista, "0", 10);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", xpath, lista, "0", 0);
            Functions.scrooll(driver, LocalizadoresBO._010414_buttonCreateUser);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "Value", LocalizadoresBO._010414_buttonCreateUser, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.Equals(driver, LocalizadoresBO._010501_user_error_text, DatosBO._010501_user_error_text);
            Functions.Equals(driver, LocalizadoresBO._010502_email_error_text, DatosBO._010502_email_error_text);
            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
            driver.Quit();

        }
    }
}
