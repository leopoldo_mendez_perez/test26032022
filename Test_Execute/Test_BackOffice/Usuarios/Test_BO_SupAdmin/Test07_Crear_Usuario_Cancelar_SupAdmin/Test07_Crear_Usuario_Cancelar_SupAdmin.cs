﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using Lucene.Net.Util;
using NUnit.Framework;
using Test_Inactivos;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test07_Crear_Usuario_Cancelar_SupAdmin
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        string Orden;
        static login login = new login();
        CancelSuperAdministrador canc= new CancelSuperAdministrador();
        private static List<String> lista = new List<String>();
      
        Lectura_Test_Inactivos lecEstado = new Lectura_Test_Inactivos();

        /*
        * CP01-07	Crear usuario (SuperAdministrador) Cancelar
        * 
        */

        [Test]
        public void Test07_Crear_Usuario_Cancelar_SupAdmin_()
        {
            if (!lecEstado.LecturaTestInactivos("Crear_Usuario_Cancelar_SupAdmin"))
            {
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                canc.altaUsuarioBOClaroCrearUsuarioBtnCancelar(driver, lista, LocalizadoresBO._010403_linkSuperAdministradores);
            }
            else
            {
                Assert.Ignore("Test ignored during Prod runs");
            }
        }

    }
}