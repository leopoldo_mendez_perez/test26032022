﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;
using Test_Inactivos;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test10_Validación_Rol_Editado_SupAdmin
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        static ValidacionEdicionUsuarioBOClaroreado suit = new ValidacionEdicionUsuarioBOClaroreado();
        login login = new login();
        private static List<String> lista = new List<String>();
      
        Lectura_Test_Inactivos lecEstado = new Lectura_Test_Inactivos();
        /*
              * CP01-10	Validacion Editar Usuario (SuperAdministrador) Edicion de Rol
              */
        [Test]
        public void Test10_Validación_Rol_Editado_SupAdmin_()
        {
            if (!lecEstado.LecturaTestInactivos("Validación_Rol_Editado_SupAdmin"))
            {
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                suit.ValidacionEdicionUsuarioBOClaroread(driver, lista, LocalizadoresBO._010908_opcionSuperAdministrador);
            }
            else
            {
                Assert.Ignore("Test ignored during Prod runs");
            }
        }

    }
}