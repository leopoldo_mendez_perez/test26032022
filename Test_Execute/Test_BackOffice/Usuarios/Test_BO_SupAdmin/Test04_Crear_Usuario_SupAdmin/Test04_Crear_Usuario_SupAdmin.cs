﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;
using Test_Inactivos;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test04_Crear_Usuario_SupAdmin
    {
   
        Lectura_Test_Inactivos lecEstado = new Lectura_Test_Inactivos();
        static ChromeDriver driver;
        static Config Conf = new Config();
         static login login = new login(); 
        Crear_Usuario_SupAdmin CrearUsuarioSuperAdmin = new Crear_Usuario_SupAdmin();
        private static List<String> lista = new List<String>();
      
            /*
            * CP01-04	Crear usuario (SuperAdministrador) Existoso (user)
            */
        [Test]
        public void Test04_Crear_Usuario_SupAdmin_()
        {
            if (!lecEstado.LecturaTestInactivos("Crear_Usuario_SupAdmin"))
            {
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                CrearUsuarioSuperAdmin.altaUsuarioBOClaro(driver, lista, DatosBO._010401_nombre_Usuario, DatosBO._010402_correo_Electronico);
            }
            else
            {
                Assert.Ignore("Test ignored during Prod runs");
            }
        }
    }
}