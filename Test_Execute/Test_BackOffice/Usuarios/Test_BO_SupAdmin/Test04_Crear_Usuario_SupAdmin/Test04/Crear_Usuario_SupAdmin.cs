﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using Test_Execute.Selenium.ResxBO;


namespace Test_Execute.Selenium.suit
{
    class Crear_Usuario_SupAdmin
    {

        Selenium Functions = new Selenium();
        string ruta = @"C:\Logs_TE_TER\";
       
        /*
         * Fecha:21.02.22
         * Creador: Leopoldo Méndez Pérez (QA)
         * Objetivo: Metodo de alta de usuario para BO 
         * Ultima actualización:
         * Responsable última actualización:
         * Objetivo última actualización:
         */
        public void altaUsuarioBOClaro(ChromeDriver driver, List<String> lista, String usuarioAdmin, String correoAdmin)
        {
            try { 
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010401_linkUsuarioHome, lista, "0", 5);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.scrooll(driver, LocalizadoresBO._010404_buttonCrearUsuario);
            Functions.ClickButton(driver, "Id", LocalizadoresBO._010402_buttonList, lista, "0", 10);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010403_linkSuperAdministradores, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._010405_nombre_Usuario, $"{usuarioAdmin}", lista, "0");
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._010406_correo_Electronico, $"{correoAdmin}", lista, "0");
            //Seleccion de Rol de superAdministrador
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010407_radioButtonPregFre, lista, "0", 0);
            Functions.scrooll(driver, LocalizadoresBO._010408_radioButtonBibl);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010408_radioButtonBibl, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010409_radioButtonTien, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010410_radioButtonUsu, lista, "0", 0);
            Functions.scrooll(driver, LocalizadoresBO._010410_radioButtonUsu);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010411_radioButtonEje, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010412_radioButtonBan, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010413_radioButtonHist, lista, "0", 0);
            Functions.ClickButton(driver, "Value", LocalizadoresBO._010414_buttonCreateUser, lista, "0", 0);
            Functions.cargando(driver,LocalizadoresBO._011005_procesando);
            Functions.ExistWhitParameter(driver,LocalizadoresBO._010415_ValidarUsuario,3);
            }
            catch (Exception e)
            {
              
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
            driver.Quit();

        }
    }
}
